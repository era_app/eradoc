//
//  FolderCell.swift
//  EraDoc
//
//  Created by SSR Lab on 10.03.22.
//

import UIKit

class FolderCell: UICollectionViewCell {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var folderNameLabel: UILabel!
    @IBOutlet weak var designView: UIView!
    @IBOutlet weak var designImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.borderWidth = 0.5
        mainView.layer.cornerRadius = 10
        mainView.layer.borderColor = UIColor.clear.cgColor
        mainView.layer.shadowColor = UIColor.systemGray.cgColor
        mainView.layer.shadowOffset = CGSize(width: 3, height: 3)
        mainView.layer.shadowOpacity = 0.2
        mainView.layer.shadowRadius = 4.0
    }

}
