//
//  HomeCell.swift
//  EraDoc
//
//  Created by SSR Lab on 10.03.22.
//

import UIKit

class HomeCell: UITableViewCell {
    @IBOutlet weak var foldersCollectionView: UICollectionView!
    @IBOutlet weak var documentsTableView: UITableView!
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    func setupCell() {
        self.selectionStyle = .none
        documentsTableView.registerCell(DocumentsCell.self)
        documentsTableView.delegate = self
        documentsTableView.dataSource = self
        documentsTableView.backgroundColor = .clear
    
        foldersCollectionView.dataSource = self
        foldersCollectionView.delegate = self
        foldersCollectionView.register(UINib(nibName: String("FolderCell"), bundle: nil), forCellWithReuseIdentifier: "FolderCell")
        
        for _ in 1...9 {
            tableViewHeight.constant += 123
        }
    }


    
}


extension HomeCell: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Tapped")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DocumentsCell.self), for: indexPath)
        guard  let documentCell = cell as? DocumentsCell else {return cell}
        return documentCell
    }
}

extension HomeCell: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FolderCell", for: indexPath) as! FolderCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let height = 100
            let width = 220
            return CGSize(width: width, height: height)     
    }
    
    
}
