//
//  HomeVC.swift
//  EraDoc
//
//  Created by SSR Lab on 10.03.22.
//

import UIKit
import BLTNBoard
import Firebase
import GoogleSignIn

class HomeVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var menuButton: UIButton!
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var menuLeading: NSLayoutConstraint!
    
    //MARK: MainView const:
    @IBOutlet weak var topView: NSLayoutConstraint!
    @IBOutlet weak var trailingView: NSLayoutConstraint!
    @IBOutlet weak var leadingView: NSLayoutConstraint!
    @IBOutlet weak var bottomView: NSLayoutConstraint!
    
    var boardManagerFirst: BLTNItemManager!
    
    @IBOutlet weak var testImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        guard let email = UserDefaults.standard.value(forKey: "email") as? String else { return }
//        DataBaseManager.shared.saveScannedDocument(userEmail: email, docURL: "qwert") { success in
//            switch success {
//            case true:
//                print("success")
//            case false:
//                print("Failure")
//            }
//        }
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setupController() {
        self.navigationItem.largeTitleDisplayMode = .never
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.setupDelegateData(self)
        tableView.registerCell(HomeCell.self)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    
    func hideMenu() {
        UIView.animate(withDuration: 0.2) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.topView.constant = 0
            sSelf.leadingView.constant = 0
            sSelf.bottomView.constant = 0
            sSelf.trailingView.constant = 0
            sSelf.menuLeading.constant = -200
            sSelf.tabBarController?.tabBar.alpha = 1
            sSelf.mainView.layer.cornerRadius = 0
            sSelf.tableView.layer.cornerRadius = 0
            sSelf.view.layoutIfNeeded()
        }
    }
    
    
    func showBuletBoard(description: String, title: String, mainButtonTitle: String, alternativeButton: String?) {
        boardManagerFirst = {
            let item = BLTNPageItem(title: title)
            item.image = self.resizeImage(image: UIImage(named: "lapp")!, targetSize: CGSize(width: 200, height: 200))
            item.actionButtonTitle = mainButtonTitle
            item.appearance.alternativeButtonTitleColor = .white
            item.appearance.actionButtonColor = UIColor(r: 72, g: 129, b: 241)
            item.alternativeHandler = { _ in
                self.boardManagerFirst.dismissBulletin()
            }
            item.actionHandler = { _ in
                self.boardManagerFirst.dismissBulletin()
                self.signOut()
            }
            
            item.descriptionText = description
            return BLTNItemManager(rootItem: item)
        }()
        boardManagerFirst.showBulletin(above: self)
    }
    
    
    func signOut() {
        do {
            try Firebase.Auth.auth().signOut()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC else { return }
            vc.modalPresentationStyle = .fullScreen
            navigationController?.present(vc, animated: true)
        }
        catch {
            print("Failed to log out")
        }
        
        GIDSignIn.sharedInstance().signOut()
    }
    
    @IBAction func foldersAction(_ sender: Any) {
        print("Folders")
    }
    
    @IBAction func logOutAction(_ sender: Any) {
        showBuletBoard(description: "Do you really want to log out of your account?", title: "Sign out", mainButtonTitle: "Sign out", alternativeButton: "Cancel")
    }
    
    
    
    
    @IBAction func slideMenuGesture(_ sender: UIPanGestureRecognizer) {
        if sender.state == .began || sender.state == .changed {
            let translation = sender.translation(in: mainView).x
            if translation > 0 { // swipe right
                if leadingView.constant == 0 {
                    UIView.animate(withDuration: 0.2) { [weak self] in
                        guard let sSelf = self else { return }
                        sSelf.topView.constant = 130
                        sSelf.leadingView.constant = 250
                        sSelf.bottomView.constant = -50
                        sSelf.trailingView.constant = 100
                        sSelf.menuLeading.constant = 20
                        sSelf.tabBarController?.tabBar.alpha = 0
                        sSelf.mainView.layer.cornerRadius = 25
                        sSelf.tableView.layer.cornerRadius = 25
                        sSelf.tableView.reloadData()
                        sSelf.view.layoutIfNeeded()
                    }
                }
                
                if leadingView.constant < 0 {
                    UIView.animate(withDuration: 0.2) { [weak self] in
                        guard let sSelf = self else { return }
                        sSelf.leadingView.constant = 0
                        sSelf.view.layoutIfNeeded()
                    }
                }
            } else { // swipe left
                if leadingView.constant == 0 {
                    print("0")
                }
                if leadingView.constant > 0 && leadingView.constant != 0 {
                    UIView.animate(withDuration: 0.2) { [weak self] in
                        guard let sSelf = self else { return }
                        sSelf.topView.constant = 0
                        sSelf.leadingView.constant = 0
                        sSelf.bottomView.constant = 0
                        sSelf.trailingView.constant = 0
                        sSelf.menuLeading.constant = -200
                        sSelf.tabBarController?.tabBar.alpha = 1
                        sSelf.mainView.layer.cornerRadius = 0
                        sSelf.tableView.layer.cornerRadius = 0
                        sSelf.view.layoutIfNeeded()
                    }
                }
            }
        } else if sender.state == .ended {
            if leadingView.constant < -100 {
                UIView.animate(withDuration: 0.2) { [weak self] in
                    guard let sSelf = self else { return }
                    sSelf.leadingView.constant += 0
                    sSelf.view.layoutIfNeeded()
                }
            }
        }
    }
    
    
    
    @IBAction func menuAction(_ sender: Any) {
        UIView.animate(withDuration: 0.2) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.topView.constant = 130
            sSelf.leadingView.constant = 250
            sSelf.bottomView.constant = -50
            sSelf.trailingView.constant = 100
            sSelf.menuLeading.constant = 20
            sSelf.mainView.layer.cornerRadius = 25
            sSelf.tableView.layer.cornerRadius = 25
            sSelf.tabBarController?.tabBar.alpha = 0
            sSelf.tableView.reloadData()
            sSelf.view.layoutIfNeeded()
        }
    }
    
    
}

extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //        guard let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC else { return }
        //        navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HomeCell.self), for: indexPath)
        guard  let homeCell = cell as? HomeCell else {return cell}
        return homeCell
    }
    
    
}
