//
//  LoginVC.swift
//  EraDoc
//
//  Created by SSR Lab on 11.03.22.
//

import UIKit
import AuthenticationServices
import Firebase
import CryptoKit
import FirebaseCore
import GoogleSignIn
import NVActivityIndicatorView
import Lottie


class LoginVC: UIViewController {
    
    var indicator: AnimationView = {
        let animation = AnimationView(name: "searchIndicator")
        animation.translatesAutoresizingMaskIntoConstraints = false
        animation.loopMode = .loop
        animation.contentMode = .scaleAspectFit
        animation.tintColor = .clear
        animation.backgroundColor = .clear
        animation.isHidden = true
        //        animation.play()
        return animation
    }()
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var appleView: UIView!
    @IBOutlet weak var googleView: UIView!
    
    @IBOutlet weak var signInButton: UIButton!
    
    fileprivate var currentNonce: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
        
    }
    
    func setupController() {
        self.view.addSubview(indicator)
        NSLayoutConstraint.activate([
            indicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 10),
            indicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0),
            indicator.widthAnchor.constraint(equalToConstant: 150),
            indicator.heightAnchor.constraint(equalToConstant: 150)
            
        ])
        
        
        
        self.hideKeyboardWhenTappedAround()
        self.addShadows(view: appleView, color: .gray)
        self.addShadows(view: googleView, color: .gray)
        self.addShadows(view: signInButton, color: .black)
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
        
    }
    
    
    
    
    private func randomNonceString(length: Int = 32) -> String {
        precondition(length > 0)
        let charset: [Character] =
        Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
        var result = ""
        var remainingLength = length
        
        while remainingLength > 0 {
            let randoms: [UInt8] = (0 ..< 16).map { _ in
                var random: UInt8 = 0
                let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                if errorCode != errSecSuccess {
                    fatalError(
                        "Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)"
                    )
                }
                return random
            }
            
            randoms.forEach { random in
                if remainingLength == 0 {
                    return
                }
                
                if random < charset.count {
                    result.append(charset[Int(random)])
                    remainingLength -= 1
                }
            }
        }
        
        return result
    }
    
    private func sha256(_ input: String) -> String {
        let inputData = Data(input.utf8)
        let hashedData = SHA256.hash(data: inputData)
        let hashString = hashedData.compactMap {
            String(format: "%02x", $0)
        }.joined()
        
        return hashString
    }
    
    func startSignInWithAppleFlow() {
        let nonce = randomNonceString()
        currentNonce = nonce
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        request.nonce = sha256(nonce)
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        //authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    func startSignInWithGoogle() {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func signFirebaseAction(_ sender: Any) {
        guard let email = emailTextField.text, !email.isEmpty else {
            self.addWarningBorder(view: emailView)
            return
        }
        
        guard let password = passwordTextField.text, !password.isEmpty else {
            self.addWarningBorder(view: passwordView)
            return }
        self.startAnimatinIndicator(indicator: indicator)
        Firebase.Auth.auth().signIn(withEmail: email, password: password) { authResult, error in
            guard let result = authResult , error == nil else {
                self.stopAnimatinIndicator(indicator: self.indicator)
                return }
            print(result)
            DataBaseManager.shared.getUser(for: DataBaseManager.safeEmail(emailAdress: email)) { result in
                switch result {
                case .success(let user):
                    print(user)
                    UserDefaults.standard.set(DataBaseManager.safeEmail(emailAdress: email), forKey: "email")
                    DispatchQueue.main.async { [weak self] in
                        guard let sSelf = self else { return }
                        sSelf.stopAnimatinIndicator(indicator: sSelf.indicator)
                        print(user)
                        let VC1 = sSelf.storyboard!.instantiateViewController(withIdentifier: "MainTabBarController") as! MainTabBarController
                        let navController = UINavigationController(rootViewController: VC1)
                        navController.modalPresentationStyle = .fullScreen
                        sSelf.present(navController, animated:true, completion: nil)
                    }
                case .failure(_):
                    self.stopAnimatinIndicator(indicator: self.indicator)
                }
            }
        }
    }
    
    @IBAction func googleSignAction(_ sender: Any) {
        startSignInWithGoogle()
    }
    
    
    @IBAction func appleSignAction(_ sender: Any) {
        startSignInWithAppleFlow()
    }
    
    
    
}

extension LoginVC: ASAuthorizationControllerDelegate {
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let nonce = currentNonce else {
                
                //                self.signInButton.isEnabled = true
                fatalError("Invalid state: A login callback was received, but no login request was sent.")
            }
            guard let appleIDToken = appleIDCredential.identityToken else {
                self.stopAnimatinIndicator(indicator: self.indicator)
                //  self.signInButton.isEnabled = true
                return
            }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                self.stopAnimatinIndicator(indicator: self.indicator)
                //     self.signInButton.isEnabled = true
                print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                return
            }
            
            self.startAnimatinIndicator(indicator: self.indicator)
            let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                      idToken: idTokenString,
                                                      rawNonce: nonce)
            // Sign in with Firebase.
            Auth.auth().signIn(with: credential) { (authResult, error) in
                if (error != nil) {
                    self.stopAnimatinIndicator(indicator: self.indicator)
                    // Error. If error.code == .MissingOrInvalidNonce, make sure
                    // you're sending the SHA256-hashed nonce as a hex string with
                    // your request to Apple.
                    //    self.signInButton.isEnabled = true
                    print(error?.localizedDescription ?? "")
                    return
                }
                guard let email = Firebase.Auth.auth().currentUser?.email else { return }
                guard let id = Firebase.Auth.auth().currentUser?.uid else { return }
                print(id)
                self.startAnimatinIndicator(indicator: self.indicator)
                
                DataBaseManager.shared.userExists(with: DataBaseManager.safeEmail(emailAdress: email)) { exist in
                    switch exist {
                    case true:
                        DataBaseManager.shared.getUser(for: DataBaseManager.safeEmail(emailAdress: email)) { result in
                            switch result {
                            case .success(let user):
                                UserDefaults.standard.set( DataBaseManager.safeEmail(emailAdress: email), forKey: "email")
                                DispatchQueue.main.async { [weak self] in
                                    guard let sSelf = self else { return }
                                    sSelf.stopAnimatinIndicator(indicator: sSelf.indicator)
                                    print(user)
                                    let VC1 = sSelf.storyboard!.instantiateViewController(withIdentifier: "MainTabBarController") as! MainTabBarController
                                    let navController = UINavigationController(rootViewController: VC1)
                                    navController.modalPresentationStyle = .fullScreen
                                    sSelf.present(navController, animated:true, completion: nil)
                                }
                            case .failure(_):
                                DispatchQueue.main.async { [weak self] in
                                    guard let sSelf = self else { return }
                                    UserDefaults.standard.setValue(DataBaseManager.safeEmail(emailAdress: email), forKey: "email")
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    guard let vc = storyboard.instantiateViewController(withIdentifier: "PersonalizationUserVC") as? PersonalizationUserVC else { return }
                                    vc.modalPresentationStyle = .fullScreen
                                    sSelf.present(vc, animated: true)
                                }
                            }
                        }
                    case false:
                        print("Error")
                        DispatchQueue.main.async { [weak self] in
                            guard let sSelf = self else { return }
                            UserDefaults.standard.setValue(DataBaseManager.safeEmail(emailAdress: email), forKey: "email")
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            guard let vc = storyboard.instantiateViewController(withIdentifier: "PersonalizationUserVC") as? PersonalizationUserVC else { return }
                            vc.modalPresentationStyle = .fullScreen
                            sSelf.present(vc, animated: true)
                        }
                        self.stopAnimatinIndicator(indicator: self.indicator)
                    }
                }
            }
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        self.stopAnimatinIndicator(indicator: self.indicator)
        print("Sign in with Apple errored: \(error.localizedDescription)")
    }
    
}


extension LoginVC: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard  error == nil  else {
            if let error = error {
                print(error)
            }
            return
        }
        
        //        self.indicator.startAnimating()
        //        self.indicator.isHidden = false
        self.signInButton.isEnabled = false
        guard let email = user.profile.email
        else { return }
//        let name = user.profile.givenName
//        let secondName = user.profile.familyName
        self.startAnimatinIndicator(indicator: self.indicator)
        DataBaseManager.shared.userExists(with: DataBaseManager.safeEmail(emailAdress: email)) { exist in
            switch exist {
            case true:
                DataBaseManager.shared.getUser(for: DataBaseManager.safeEmail(emailAdress: email)) { result in
                    switch result {
                    case .success(let user):
                        UserDefaults.standard.set( DataBaseManager.safeEmail(emailAdress: email), forKey: "email")
                        DispatchQueue.main.async { [weak self] in
                            guard let sSelf = self else { return }
                            sSelf.stopAnimatinIndicator(indicator: sSelf.indicator)
                            print(user)
                            let VC1 = sSelf.storyboard!.instantiateViewController(withIdentifier: "MainTabBarController") as! MainTabBarController
                            let navController = UINavigationController(rootViewController: VC1)
                            navController.modalPresentationStyle = .fullScreen
                            sSelf.present(navController, animated:true, completion: nil)
                        }
                    case .failure(_):
                        DispatchQueue.main.async { [weak self] in
                            guard let sSelf = self else { return }
                            UserDefaults.standard.setValue(DataBaseManager.safeEmail(emailAdress: email), forKey: "email")
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            guard let vc = storyboard.instantiateViewController(withIdentifier: "PersonalizationUserVC") as? PersonalizationUserVC else { return }
                            vc.modalPresentationStyle = .fullScreen
                            sSelf.present(vc, animated: true)
                        }
                    }
                }
            case false:
                print("Error")
                self.stopAnimatinIndicator(indicator: self.indicator)
            }
        }
        
        guard let athentification = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: athentification.idToken, accessToken: athentification.accessToken)
        Firebase.Auth.auth().signIn(with: credential, completion: {authResult, error in
            guard authResult != nil , error == nil  else {
                self.signInButton.isEnabled = true
                return
            }
            self.signInButton.isEnabled = true
            print("SUCCESS SIGN IN")
            self.stopAnimatinIndicator(indicator: self.indicator)
            
        })
        
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error! ) {
        self.stopAnimatinIndicator(indicator: self.indicator)
    }
}

extension LoginVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.emailView.layer.borderWidth = 0
            sSelf.passwordView.layer.borderWidth = 0
        }
        
    }
}
