//
//  PersonalizationUserVC.swift
//  EraDoc
//
//  Created by Ilya Rabyko on 23.04.22.
//

import UIKit
import Lottie
import Firebase

class PersonalizationUserVC: UIViewController {
    
    
    var indicator: AnimationView = {
        let animation = AnimationView(name: "searchIndicator")
        animation.translatesAutoresizingMaskIntoConstraints = false
        animation.loopMode = .loop
        animation.contentMode = .scaleAspectFit
        animation.tintColor = .clear
        animation.backgroundColor = .clear
        animation.isHidden = true
//        animation.play()
        return animation
    }()

    @IBOutlet weak var continueView: UIView!
    
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var surnameView: UIView!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    func setupController() {
        self.view.addSubview(indicator)
        NSLayoutConstraint.activate([
            indicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 10),
            indicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0),
            indicator.widthAnchor.constraint(equalToConstant: 150),
            indicator.heightAnchor.constraint(equalToConstant: 150)
            
        ])
        
        
        nameTextField.delegate = self
        surnameTextField.delegate = self
        continueView.addShadows(color: .gray)
        
    }
    
    @IBAction func continueAction(_ sender: Any) {
        guard let name = nameTextField.text, !name.isEmpty else {
            nameView.addWarningBorder()
            return }
        
        guard let surname = surnameTextField.text, !surname.isEmpty else {
            surnameView.addWarningBorder()
            return }
        
        guard let email = UserDefaults.standard.value(forKey: "email") as? String else { return }
        
        self.startAnimatinIndicator(indicator: indicator)
        let docUser = EraDocUser(email: email, name: name, surname: surname, userImage: "")
        DataBaseManager.shared.insertUser(with: docUser) { result in
            switch result {
            case true:
                self.stopAnimatinIndicator(indicator: self.indicator)
                UserDefaults.standard.set( DataBaseManager.safeEmail(emailAdress: email), forKey: "email")
                DispatchQueue.main.async { [weak self] in
                    guard let sSelf = self else { return }
                    sSelf.startAnimatinIndicator(indicator: sSelf.indicator)
                    let VC1 = sSelf.storyboard!.instantiateViewController(withIdentifier: "MainTabBarController") as! MainTabBarController
                    let navController = UINavigationController(rootViewController: VC1)
                    navController.modalPresentationStyle = .fullScreen
                    sSelf.present(navController, animated:true, completion: nil)
                    
                    
                }
            case false:
                self.stopAnimatinIndicator(indicator: self.indicator)
            }
        }
        
    }
    
}


extension PersonalizationUserVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.nameView.layer.borderWidth = 0
            sSelf.surnameView.layer.borderWidth = 0
        }
        
    }
}


//
//
//DataBaseManager.shared.insertUser(with: eraDocUser) { result in
//    switch result {
//    case true:
//        self.stopAnimatinIndicator(indicator: self.indicator)
//        UserDefaults.standard.set( DataBaseManager.safeEmail(emailAdress: email), forKey: "email")
//        DispatchQueue.main.async { [weak self] in
//            guard let sSelf = self else { return }
//            let vc = PersonalizationUserVC(nibName: "PersonalizationUserVC", bundle: nil)
//            vc.modalPresentationStyle = .fullScreen
//            sSelf.present(vc, animated: true)
//        }
//    case false:
//        self.stopAnimatinIndicator(indicator: self.indicator)
//    }
//}
