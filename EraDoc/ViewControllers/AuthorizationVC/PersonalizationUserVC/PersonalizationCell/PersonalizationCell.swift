//
//  PersonalizationCell.swift
//  EraDoc
//
//  Created by Ilya Rabyko on 23.04.22.
//

import UIKit

class PersonalizationCell: UITableViewCell {
    @IBOutlet weak var userImageView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    func setupCell() {
        self.selectionStyle = .none
    }

    
    
}
