//
//  FourthPage.swift
//  EraDoc
//
//  Created by SSR Lab on 11.03.22.
//

import UIKit
//No need for expensive legal services to conclude a contract
class FourthPage: UIViewController {
    
    private lazy var logoImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = UIImage(named: "LAPP-2")
        return image
    }()
    
    private lazy var label: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.text = "No need for expensive legal services to conclude a contract"
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 20, weight: .thin)
        return label
    }()
    
    private lazy var mainImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = UIImage(named: "page3")
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    
    @IBOutlet weak var mainLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor =  UIColor(r: 72, g: 129, b: 241)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupView()
    }

    //Page1
    
    func setupView() {
        self.view.addSubview(label)
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 30),
            label.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -30),
            label.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -70),
            label.heightAnchor.constraint(equalToConstant: 50)

        
        ])
        
        self.view.addSubview(logoImage)
        NSLayoutConstraint.activate([
            logoImage.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 70),
            logoImage.widthAnchor.constraint(equalToConstant: 200),
            logoImage.heightAnchor.constraint(equalToConstant: 50),
            logoImage.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        ])
        
        self.view.addSubview(mainImage)
        NSLayoutConstraint.activate([
            mainImage.topAnchor.constraint(equalTo: self.logoImage.bottomAnchor, constant: 30),
            mainImage.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20),
            mainImage.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20),
            mainImage.bottomAnchor.constraint(equalTo: self.label.topAnchor, constant: -30)
            
        ])
    }
    
    @IBAction func lol(_ sender: Any) {
        let vc = HomeVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
