//
//  InformationVC.swift
//  EraDoc
//
//  Created by SSR Lab on 11.03.22.
//

import UIKit
import liquid_swipe
class InformationVC: LiquidSwipeContainerController, LiquidSwipeContainerDataSource {
    
    var viewControllers: [UIViewController] = {
        let firstPageVC = FirstPage(nibName: "FirstPage", bundle: nil)
        let secondPageVC = SecondPage()
        let thirdPageVC = ThirdPage()
        let fourthPageVC = FourthPage()
        let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC")
        
        var controllers: [UIViewController] = [firstPageVC, secondPageVC, thirdPageVC, fourthPageVC, loginVC]
        return controllers
    } ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datasource = self
    }

    func numberOfControllersInLiquidSwipeContainer(_ liquidSwipeContainer: LiquidSwipeContainerController) -> Int {
        return viewControllers.count
    }
    
    func liquidSwipeContainer(_ liquidSwipeContainer: LiquidSwipeContainerController, viewControllerAtIndex index: Int) -> UIViewController {
        return viewControllers[index]
    }

}

