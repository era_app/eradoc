//
//  NewDocumentVC.swift
//  EraDoc
//
//  Created by SSR Lab on 15.03.22.
//

import UIKit
import VisionKit
import Vision
import BLTNBoard


class NewDocumentVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var boardManagerFirst: BLTNItemManager!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.largeTitleDisplayMode = .never
        setupController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    func showBuletBoard(description: String) {
        boardManagerFirst = {
            let item = BLTNPageItem(title: "Error")
            item.image = self.resizeImage(image: UIImage(named: "lapp")!, targetSize: CGSize(width: 200, height: 200))
            item.actionButtonTitle = "Hide"
            item.appearance.alternativeButtonTitleColor = .white
            item.appearance.actionButtonColor = UIColor(r: 72, g: 129, b: 241)
            item.actionHandler = { _ in
                self.boardManagerFirst.dismissBulletin()
            }
            item.descriptionText = description
            return BLTNItemManager(rootItem: item)
        }()
        boardManagerFirst.showBulletin(above: self)
    }
    
    func recognizeText(image: UIImage?) {
        guard let cgImage = image?.cgImage else { return }
        
        let handler = VNImageRequestHandler(cgImage: cgImage, options: [:])
        
        let request = VNRecognizeTextRequest { request, error in
            guard let observation = request.results as? [VNRecognizedTextObservation], error == nil else { return }
            
            
            let text = observation.compactMap({
                $0.topCandidates(1).first?.string
            }).joined(separator: ", ")
            
            DispatchQueue.main.async {
                let vc = ScannerResultVC(nibName: "ScannerResultVC", bundle: nil)
                vc.image = image
                vc.text = text
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        do {
            try handler.perform([request])
        }
        catch {
            showBuletBoard(description: error.localizedDescription)
        }
    }
    
    
    
    
    
    
    func setupController() {
        tableView.setupDelegateData(self)
        tableView.registerCell(NewDocCell.self)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @objc func openScanner() {
        let scannerViewController = VNDocumentCameraViewController()
        scannerViewController.delegate = self
        present(scannerViewController, animated: true)
    }
    

    
    
}
extension NewDocumentVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let vc = ResidentsListAgreementVC(nibName: "ResidentsListAgreementVC", bundle: nil)
        //        navigationController?.pushViewController(vc, animated: true)
        //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //        guard let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC else { return }
        //        navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NewDocCell.self), for: indexPath)
        guard  let newDocCell = cell as? NewDocCell else { return cell }
        newDocCell.scannerButton.addTarget(self, action: #selector(openScanner), for: .touchUpInside)
        return newDocCell
    }
    
    
}


extension NewDocumentVC: VNDocumentCameraViewControllerDelegate {
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
        var int = 0
        for pageNumber in 0..<scan.pageCount {
            if int == 0 {
                int += 1
                let image = scan.imageOfPage(at: pageNumber)
                self.recognizeText(image: image)
            }
        }
        controller.dismiss(animated: true)
    }
    
    func documentCameraViewControllerDidCancel(_ controller: VNDocumentCameraViewController) {
        controller.dismiss(animated: true)
    }
    
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFailWithError error: Error) {
        showBuletBoard(description: error.localizedDescription)
        controller.dismiss(animated: true)
    }
}
