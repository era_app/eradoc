//
//  ScannerResultCell.swift
//  EraDoc
//
//  Created by Ilya Rabyko on 21.04.22.
//

import UIKit

class ScannerResultCell: UITableViewCell {
    
    @IBOutlet weak var backgroundImageView: UIView!
    @IBOutlet weak var recognizeButtonView: UIView!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var saveScannedDocumentButton: UIView!
    
    @IBOutlet weak var scanImage: UIImageView!
    
    @IBOutlet weak var recognizeButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    
    @IBOutlet weak var recognizeLabelTextLabel: UILabel!
    @IBOutlet weak var backViewImageHeight: NSLayoutConstraint!
    @IBOutlet weak var backViewImageWidth: NSLayoutConstraint!
    
    var isScannedText: Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    func setupCell(image: UIImage, text: String?) {
        self.selectionStyle = .none
        backgroundImageView.addShadows(color: UIColor.gray)
        shareView.addShadows(color: UIColor.gray)
        recognizeButtonView.addShadows(color: UIColor.gray)
        saveScannedDocumentButton.addShadows(color: UIColor.gray)
        self.scanImage.image = image
        self.backViewImageHeight.constant = self.scanImage.contentClippingRect.height
        self.backViewImageWidth.constant = self.scanImage.contentClippingRect.width
        guard let recognizeText = text else {
            recognizeButton.setTitle("Recognize text", for: .normal)
            self.recognizeLabelTextLabel.text = ""
            return
        }
        self.recognizeButton.setTitle("Hide", for: .normal)
        self.recognizeLabelTextLabel.text = recognizeText

        print(self.scanImage.contentClippingRect)
    }
    
}
