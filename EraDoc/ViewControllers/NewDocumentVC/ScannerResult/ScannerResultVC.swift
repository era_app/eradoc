//
//  ScannerResultVC.swift
//  EraDoc
//
//  Created by Ilya Rabyko on 21.04.22.
//

import UIKit
import BLTNBoard
import Lottie

class ScannerResultVC: UIViewController {
    
    
    var indicator: AnimationView = {
        let animation = AnimationView(name: "searchIndicator")
        animation.translatesAutoresizingMaskIntoConstraints = false
        animation.loopMode = .loop
        animation.contentMode = .scaleAspectFit
        animation.tintColor = .clear
        animation.backgroundColor = .clear
        animation.isHidden = true
        //        animation.play()
        return animation
    }()
    
    
    var doneAnimaiton: AnimationView = {
        let animation = AnimationView(name: "doneAnimation")
        animation.translatesAutoresizingMaskIntoConstraints = false
        animation.loopMode = .loop
        animation.contentMode = .scaleAspectFit
        animation.tintColor = .clear
        animation.backgroundColor = .clear
        return animation
    }()
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var copiedLabel: UILabel!
    @IBOutlet weak var coppiedView: UIView!
    @IBOutlet weak var copiedViewBottomConstr: NSLayoutConstraint!
    var image: UIImage!
    var text: String!
    
    var boardManagerFirst: BLTNItemManager!
    @IBOutlet weak var doneAnimationView: AnimationView!
    
    var isTextScanned: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.doneAnimaiton.stop()
    }
    
    func setupController() {
        self.navigationController?.navigationBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = false
        tableView.registerCell(ScannerResultCell.self)
        tableView.setupDelegateData(self)
        

        coppiedView.addShadows(color: UIColor.gray)
        coppiedView.layer.borderColor = UIColor(r: 65, g: 65, b: 110).cgColor
        coppiedView.layer.borderWidth = 1
        self.view.addSubview(doneAnimaiton)
        NSLayoutConstraint.activate([
            doneAnimaiton.centerYAnchor.constraint(equalTo: self.coppiedView.centerYAnchor, constant: 0),
            doneAnimaiton.trailingAnchor.constraint(equalTo: self.coppiedView.trailingAnchor, constant: -10),
            doneAnimaiton.widthAnchor.constraint(equalToConstant: 50),
            doneAnimaiton.heightAnchor.constraint(equalToConstant: 50)
        ])
        
        self.view.addSubview(indicator)
        NSLayoutConstraint.activate([
            indicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 10),
            indicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0),
            indicator.widthAnchor.constraint(equalToConstant: 150),
            indicator.heightAnchor.constraint(equalToConstant: 150)
            
        ])
        
        
        
    }
    
    func clipBoardAnimation() {
        self.copiedLabel.text = "Copied"
        UIPasteboard.general.string = text
        UIView.animate(withDuration: 0.7, delay: 0.5, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.5, options: .curveEaseIn) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.coppiedView.alpha = 1
            sSelf.doneAnimaiton.play()
            sSelf.copiedViewBottomConstr.constant = 30
            sSelf.view.layoutIfNeeded()
        } completion: { _ in
            UIView.animate(withDuration: 0.7, delay: 2, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.5, options: .curveEaseIn) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.coppiedView.alpha = 0
                sSelf.copiedViewBottomConstr.constant = -100
                sSelf.view.layoutIfNeeded()
            }
        }
    }
    
    
    @objc func scanText() {
        if !isTextScanned {
            self.isTextScanned = true
            UIView.transition(with: self.tableView, duration: 0.3, options: .transitionCrossDissolve) { [weak self] in
                guard let sSelf = self else { return }
                DispatchQueue.main.async {
                    sSelf.tableView.beginUpdates()
                    sSelf.tableView.rowHeight = UITableView.automaticDimension
                    sSelf.tableView.reloadData()
                    sSelf.tableView.endUpdates()
                }
            }
        } else {
            self.isTextScanned = false
            UIView.transition(with: self.tableView, duration: 0.3, options: .transitionCrossDissolve) { [weak self] in
                guard let sSelf = self else { return }
                DispatchQueue.main.async {
                    sSelf.tableView.beginUpdates()
                    sSelf.tableView.rowHeight = UITableView.automaticDimension
                    sSelf.tableView.reloadData()
                    sSelf.tableView.endUpdates()
                }
            }
        }
        
    }
    
    
    @objc func saveScannedDocument() {
        guard let email = UserDefaults.standard.value(forKey: "email") as? String else { return }
        guard let data = self.image.pngData() else { return }
        self.startAnimatinIndicator(indicator: indicator)
        StorageManager.shared.uploadScannedImage(with: data, email: email) { result in
            switch result {
            case .success(let url):
                DataBaseManager.shared.saveScannedDocument(userEmail: email, docURL: url) { result in
                    switch result {
                    case true:
                        DispatchQueue.main.async { [weak self] in
                            guard let sSelf = self else { return }
                            sSelf.copiedLabel.text = "Saved"
                            sSelf.stopAnimatinIndicator(indicator: sSelf.indicator)
                            UIView.animate(withDuration: 0.7, delay: 0.5, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.5, options: .curveEaseIn) { [weak self] in
                                guard let sSelf = self else { return }
                                sSelf.coppiedView.alpha = 1
                                sSelf.doneAnimaiton.play()
                                sSelf.copiedViewBottomConstr.constant = 30
                                sSelf.view.layoutIfNeeded()
                            } completion: { _ in
                                UIView.animate(withDuration: 0.7, delay: 2, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.5, options: .curveEaseIn) { [weak self] in
                                    guard let sSelf = self else { return }
                                    sSelf.coppiedView.alpha = 0
                                    sSelf.copiedViewBottomConstr.constant = -100
                                    sSelf.view.layoutIfNeeded()
                                }
                            }
                        }
                    case false:
                        self.stopAnimatinIndicator(indicator: self.indicator)
                    }
                }
            case .failure(_):
                self.stopAnimatinIndicator(indicator: self.indicator)
            }
        }
           

    }
    
    @objc func showBuletBoard() {
        boardManagerFirst = {
            let item = BLTNPageItem(title: "Share")
            item.image = self.resizeImage(image: UIImage(named: "lapp")!, targetSize: CGSize(width: 200, height: 200))
            item.actionButtonTitle = "Share document"
            item.appearance.alternativeButtonTitleColor = UIColor(r: 72, g: 129, b: 241)
            item.alternativeButtonTitle = "Copy recognized text"
            item.appearance.actionButtonColor = UIColor(r: 72, g: 129, b: 241)
            item.alternativeHandler = { _ in
                self.boardManagerFirst.dismissBulletin()
                self.clipBoardAnimation()
            }
            item.actionHandler = { _ in
                self.boardManagerFirst.dismissBulletin()
                self.shareImageButton()
            }
            
            item.descriptionText = "Please, choose action."
            return BLTNItemManager(rootItem: item)
        }()
        boardManagerFirst.showBulletin(above: self)
    }
    
    
    func shareImageButton() {
        let imageToShare = [self.image]
        let activityViewController = UIActivityViewController(activityItems: imageToShare as [Any], applicationActivities: nil)
        activityViewController.title = "LLAP"
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook, UIActivity.ActivityType.mail ]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
}

extension ScannerResultVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ScannerResultCell.self), for: indexPath)
        guard  let scanCell = cell as? ScannerResultCell else { return cell }
        scanCell.recognizeButton.addTarget(self, action: #selector(scanText), for: .touchUpInside)
        scanCell.shareButton.addTarget(self, action: #selector(showBuletBoard), for: .touchUpInside)
        scanCell.saveButton.addTarget(self, action: #selector(saveScannedDocument), for: .touchUpInside)
        if isTextScanned {
            scanCell.setupCell(image: self.image, text: self.text)
        } else {
            scanCell.setupCell(image: self.image, text: nil)
        }
        return scanCell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
