//
//  NewDocCell.swift
//  EraDoc
//
//  Created by SSR Lab on 15.03.22.
//

import UIKit

class NewDocCell: UITableViewCell {

    @IBOutlet weak var scannerView: UIView!
    @IBOutlet weak var contractsView: UIView!
    @IBOutlet weak var physView: UIView!
    @IBOutlet weak var legalView: UIView!
    @IBOutlet weak var agreementsView: UIView!
    @IBOutlet weak var reportView: UIView!
    
    @IBOutlet weak var scannerButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        setupView(view: scannerView)
        setupView(view: contractsView)
        setupView(view: physView)
        setupView(view: legalView)
        setupView(view: agreementsView)
        setupView(view: reportView)
    }

    func setupView(view: UIView) {
        view.layer.borderWidth = 0.5
        view.layer.cornerRadius = 10
        view.layer.borderColor = UIColor.clear.cgColor
        view.layer.shadowColor = UIColor.systemGray.cgColor
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
        view.layer.shadowOpacity = 0.1
        view.layer.shadowRadius = 4.0
    }
    
}

//setupView(view: contractsView, color: .systemGreen)
//setupView(view: physView, color: .systemIndigo)
//setupView(view: legalView, color: .systemRed)
//setupView(view: agreementsView, color: .systemYellow)
//setupView(view: reportView, color: .systemTeal)
