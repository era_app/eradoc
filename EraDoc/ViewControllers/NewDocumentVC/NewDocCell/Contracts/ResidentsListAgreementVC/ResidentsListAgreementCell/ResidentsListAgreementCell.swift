//
//  ResidentsListAgreementCell.swift
//  EraDoc
//
//  Created by SSR Lab on 19.04.22.
//

import UIKit

class ResidentsListAgreementCell: UITableViewCell {

    
    @IBOutlet var collection: [UIView]!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupShadow()
    }


    func setupShadow() {
        for view in collection {
            view.layer.borderWidth = 0.5
            view.layer.cornerRadius = 10
            view.layer.borderColor = UIColor.clear.cgColor
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOffset = CGSize(width: 3, height: 3)
            view.layer.shadowOpacity = 0.1
            view.layer.shadowRadius = 4.0
        }
    }
    
}
