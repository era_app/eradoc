//
//  ResidentsListAgreementVC.swift
//  EraDoc
//
//  Created by SSR Lab on 19.04.22.
//

import UIKit

class ResidentsListAgreementVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
    
    func setupController() {
        tableView.rowHeight = 8000
        tableView.setupDelegateData(self)
        tableView.registerCell(ResidentsListAgreementCell.self)
    }




}

extension ResidentsListAgreementVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          print("Error")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ResidentsListAgreementCell.self), for: indexPath)
        guard  let residentsListAgreementCell = cell as? ResidentsListAgreementCell else {return cell}
        return residentsListAgreementCell
    }
    
    
}
