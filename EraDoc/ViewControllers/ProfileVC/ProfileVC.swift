//
//  ProfileVC.swift
//  EraDoc
//
//  Created by Ilya Rabyko on 28.04.22.
//

import UIKit

class ProfileVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
    
    
    
    func setupController() {
        self.navigationItem.largeTitleDisplayMode = .never
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.setupDelegateData(self)
        tableView.registerCell(ProfileCell.self)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
}


extension ProfileVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProfileCell.self), for: indexPath)
        guard  let profileCell = cell as? ProfileCell else {return cell}
        return profileCell
    }
    
    
}
