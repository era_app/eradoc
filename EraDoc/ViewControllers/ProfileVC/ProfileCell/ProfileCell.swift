//
//  ProfileCell.swift
//  EraDoc
//
//  Created by Ilya Rabyko on 28.04.22.
//

import UIKit

class ProfileCell: UITableViewCell {
    
    @IBOutlet weak var scannedDocumentsView: UIView!
    
    @IBOutlet var allViews: [UIView]!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        setupShadow()
    }

    
    
    func setupShadow() {
        for view in allViews {
            view.addShadows(color: UIColor.gray)
        }
    }
    
}
