//
//  PreviewVC.swift
//  EraDoc
//
//  Created by SSR Lab on 11.03.22.
//

import UIKit
import Lottie
import Firebase

class PreviewVC: UIViewController {
    
    @IBOutlet weak var logoImage: UIImageView!
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        hideImage()
    }
    
    func hideImage() {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.logoImage.alpha = 0
        } completion: { _ in
            self.checkUser()
        }
    }
    
    
    
    
    func checkUser() {
        if Firebase.Auth.auth().currentUser != nil {
            guard let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "MainTabBarController") as? MainTabBarController else { return }
            let navController = UINavigationController(rootViewController: VC1)
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated: true, completion: nil)
        } else {
            guard let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "InformationVC") as? InformationVC else { return }
            let navController = UINavigationController(rootViewController: VC1)
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    
    
}
