//
//  Objects.swift
//  EraDoc
//
//  Created by SSR Lab on 18.04.22.
//

import Foundation
import UIKit

struct EraDocUser {
    let email: String
    let name: String
    let surname: String
    let userImage: String
}

struct ScannedDocument {
    let scannedDocumten: UIImage
}
