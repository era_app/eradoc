//
//  DatabaseManager.swift
//  EraDoc
//
//  Created by SSR Lab on 18.04.22.
//

import Foundation
import Firebase
final class DataBaseManager {
    static let shared = DataBaseManager()
    
    private let database = Database.database().reference()
    
    static func safeEmail(emailAdress: String) -> String {
        let safeEmail = emailAdress.replacingOccurrences(of: ".", with: "-")
        let safe = safeEmail.replacingOccurrences(of: "/", with: "_")
        return safe
    }

    }
    

extension DataBaseManager {
    public func userExists(with email: String, completion: @escaping((Bool) -> Void)) {
        database.child("Accounts/\(email)").observeSingleEvent(of: .value, with: { snapshot in
            guard snapshot.value as? String != nil else {
                completion(false)
                return
            }
            completion(true)
        })
    }
    
    public func getData(for email: String, completion: @escaping (Result<Any, Error>) -> Void) {
        self.database.child("Accounts/\(email)").observeSingleEvent(of: .value) { snapshot in
            guard let value = snapshot.value else {
                completion(.failure(DatabaseError.FailedToFetch))
                return
            }
            completion(.success(value))
        }
    }
    
    public func getUser(for email: String, completion: @escaping (Result<EraDocUser, Error>) -> Void) {
        self.database.child("Accounts/\(email)").observeSingleEvent(of: .value) { snapshot in
            guard let value = snapshot.value as? [String: Any] else {
                completion(.failure(DatabaseError.FailedToFetch))
                return
            }
            
            guard let name = value["name"] as? String else { return }
            guard let surname = value["surname"] as? String else { return }
            guard let email = value["email"] as? String else { return }
            guard let image = value["user_image"] as? String else { return }
            completion(.success(EraDocUser(email: email, name: name, surname: surname, userImage: image )))
        }
    }
    
    
    public func getScannedDocuments(email: String, completion: @escaping(Result<[ScannedDocument], Error>) -> Void ) {
        Database.database().reference().child("Accounts/\(email)/scanned_documents").observe( .value, with: { snapshot in
            var documents: [ScannedDocument] = []
            for item in snapshot.children {
                let chats = item as! DataSnapshot
                if let chat = chats.value as? [String: Any] {
                    guard let name = chat["document"] as? String else {
                        completion(.failure(DatabaseError.FailedToFetch))
                        return }
                    guard let data =  Data(base64Encoded: name) else { return }
                    guard let image = UIImage(data: data)  else { return }
                    documents.append(ScannedDocument(scannedDocumten: image))
                }
                
            }
            completion(.success(documents))
        })
    }
    
    
    public func saveScannedDocument(userEmail: String, docURL: String , completion: @escaping (Bool) -> Void ) {
        let friendToken = Int.random(in: 0...999999999999)
        let newElement =
        ["document": docURL]
        
        Database.database().reference().child("Accounts/\(userEmail)/scanned_documents/\(friendToken)").setValue(newElement, withCompletionBlock: { error, _ in
            guard error == nil else {
                completion(false)
                print(error as Any)
                return
            }
        })
        completion(true)
    }
    
    
    
    
    
    public func insertUser(with user: EraDocUser, completion: @escaping (Bool) -> Void ) {
        database.child("Accounts/\(user.email)").setValue([
            "email": DataBaseManager.safeEmail(emailAdress: user.email),
            "name":  user.name,
            "surname": user.surname,
            "user_image": ""
            
            
        ], withCompletionBlock: { error , _ in
            guard error == nil else {
                print("Failed to write Database")
                completion(false)
                return
            }
            
            self.database.child("users").observeSingleEvent(of: .value, with: { snapshot in
                if var userCollection = snapshot.value as? [[String: String]] {
                    // append to user dictionary
                    let newElement =
                    ["name": user.name ,
                     "email": user.email
                    ]
                    
                    userCollection.append(newElement)
                    
                    self.database.child("users").setValue(userCollection, withCompletionBlock: { error, _ in
                        guard error == nil else {
                            completion(false)
                            return
                        }
                        completion(true)
                    })
                }
                else {
                    // create that dictionary
                    let newCollection: [[String: String]] = [
                        [ "name": user.name ,
                          "email": user.email
                        ]
                    ]
                    self.database.child("users").setValue(newCollection, withCompletionBlock: { error, _ in
                        guard error == nil else {
                            completion(false)
                            return
                        }
                        completion(true)
                    })
                }
            })
            
        })
    }
    
    public enum DatabaseError: Error {
        case FailedToFetch
    }
    
    
}


