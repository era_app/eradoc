//
//  StorageManager.swift
//  EraDoc
//
//  Created by Ilya Rabyko on 4.05.22.
//

import Foundation
import Firebase
final class StorageManager {
    static let shared = StorageManager()
    
    private let storage = Storage.storage().reference()
    
    public typealias uploadImage = (Result<String, Error>) -> Void
    
    public func uploadScannedImage(with data: Data, email: String, completion: @escaping uploadImage) {
        let random: Int = Int.random(in: 0...9999999999)
        storage.child("\(email)/scannedDocuments/\(random)").putData(data, metadata: nil, completion: { [weak self] metadata, error in
            guard let strongSelf = self else {
                return
            }
            guard error == nil else {
                // failed
                print("failed to upload data to firebase for picture")
                completion(.failure(StorageErrors.failedToUpload))
                return
            }
            
            strongSelf.storage.child("\(email)/scannedDocuments").downloadURL(completion: { url, error in
                guard let url = url else {
                    print("Failed to get download url")
                    completion(.failure(StorageErrors.failedToGetDownloadURL))
                    return
                }
                
                let urlString = url.absoluteString
                print("download url returned: \(urlString)")
                completion(.success(urlString))
            })
        })
    }
    
    
    public enum StorageErrors: Error {
        case failedToUpload
        case failedToGetDownloadURL
    }
    
    
}
