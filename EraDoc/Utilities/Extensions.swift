//
//  Extensions.swift
//  EraDoc
//
//  Created by SSR Lab on 10.03.22.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import Lottie
extension UIView {
    var heightConstraint: NSLayoutConstraint? {
        get {
            return constraints.first(where: {
                $0.firstAttribute == .height && $0.relation == .equal
            })
        }
        set { setNeedsLayout() }
    }
    
    var widthConstraint: NSLayoutConstraint? {
        get {
            return constraints.first(where: {
                $0.firstAttribute == .width && $0.relation == .equal
            })
        }
        set { setNeedsLayout() }
    }
    
    var bottomConstraint: NSLayoutConstraint? {
        get {
            return constraints.first(where: {
                $0.firstAttribute == .bottom && $0.relation == .equal
            })
        }
        set { setNeedsLayout() }
    }
    
    var topConstraint: NSLayoutConstraint? {
        get {
            return constraints.first(where: {
                $0.firstAttribute == .top && $0.relation == .equal
            })
        }
        set { setNeedsLayout() }
    }
    
    var trailingConstraint: NSLayoutConstraint? {
        get {
            return constraints.first(where: {
                $0.firstAttribute == .trailing && $0.relation == .equal
            })
        }
        set { setNeedsLayout() }
    }
    
    var leadingConstraint: NSLayoutConstraint? {
        get {
            return constraints.first(where: {
                $0.firstAttribute == .leading && $0.relation == .equal
            })
        }
        set { setNeedsLayout() }
    }
    
}

extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    
    func addWarningBorder() {
        UIView.animate(withDuration: 0.3) {
            self.layer.borderWidth = 1
            self.layer.borderColor = UIColor.red.cgColor
        }
    }
    
    
    func addShadows(color: UIColor) {
        layer.borderWidth = 0.5
        layer.cornerRadius = 10
        layer.borderColor = UIColor.clear.cgColor
        layer.shadowColor = color.cgColor
        layer.shadowOffset = CGSize(width: 3, height: 3)
        layer.shadowOpacity = 0.1
        layer.shadowRadius = 4.0
    }
    
    
}

extension UITableView {
    func registerCell(_ cellClass: AnyClass) {
        let nib = UINib(nibName: String(describing: cellClass.self), bundle: nil)
        self.register(nib, forCellReuseIdentifier: String(describing: cellClass.self))
    }
    
    func setupDelegateData(_ controller: UIViewController) {
        self.delegate = controller as? UITableViewDelegate
        self.dataSource = controller as? UITableViewDataSource
        self.tableFooterView = UIView()
    }
}

extension UIColor {
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
}

extension UIViewController {
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let image = newImage else {
            return  UIImage(systemName: "heart")!}
        
        return image
    }
    
    
    func startAnimatinIndicator(indicator: AnimationView) {
        indicator.isHidden = false
        view.isUserInteractionEnabled = false
        indicator.alpha = 0
        UIView.animate(withDuration: 0.3) {
            indicator.alpha = 1
            DispatchQueue.main.async {
                indicator.play()
            }
        }
        
    }
    
    func stopAnimatinIndicator(indicator: AnimationView) {
        view.isUserInteractionEnabled = true
        UIView.animate(withDuration: 0.3) {
            indicator.alpha = 0
            DispatchQueue.main.async {
                indicator.isHidden = true
                indicator.stop()
            }
        }
    }
    
    
    func setupIndicator(indicator: NVActivityIndicatorView, color: UIColor) {
        indicator.type = .ballTrianglePath
        indicator.backgroundColor = .clear
        indicator.color = color
    }
    
    func addWarningBorder(view: UIView) {
        UIView.animate(withDuration: 0.3) {
            view.layer.borderWidth = 1
            view.layer.borderColor = UIColor.red.cgColor
        }
    }
    
    
    func addShadows(view: UIView, color: UIColor) {
        view.layer.borderWidth = 0.5
        view.layer.cornerRadius = 10
        view.layer.borderColor = UIColor.clear.cgColor
        view.layer.shadowColor = color.cgColor
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
        view.layer.shadowOpacity = 0.1
        view.layer.shadowRadius = 4.0
    }
    
    @objc func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:    #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}


extension UIImageView {
    var contentClippingRect: CGRect {
        guard let image = image else { return bounds }
        guard contentMode == .scaleAspectFit else { return bounds }
        guard image.size.width > 0 && image.size.height > 0 else { return bounds }

        let scale: CGFloat
        if image.size.width > image.size.height {
            scale = bounds.width / image.size.width
        } else {
            scale = bounds.height / image.size.height
        }

        let size = CGSize(width: image.size.width * scale, height: image.size.height * scale)
        let x = (bounds.width - size.width) / 2.0
        let y = (bounds.height - size.height) / 2.0

        return CGRect(x: x, y: y, width: size.width, height: size.height)
    }
}
